var manager =
{
	init: function()
	{
		manager.bst = new bst();
		
		manager.bst.addChild(25);
		manager.bst.addChild(15);
		manager.bst.addChild(50);
		manager.bst.addChild(10);
		manager.bst.addChild(22);
		manager.bst.addChild(35);
		manager.bst.addChild(70);
		
		//ref inputs
		manager.input_search = $("#input_search")[0];
		manager.input_insert = $("#input_insert")[0];
		manager.input_delete = $("#input_delete")[0];
		
		//ref outputs
		manager.output_search = $("#output_search")[0];
		manager.output_insert = $("#output_insert")[0];
		manager.output_delete = $("#output_delete")[0];
	},
	search: function()
	{
		var obj = manager.bst.searchChild(parseInt(manager.input_search.value));
		manager.output_search.innerHTML = JSON.stringify(obj);
	},
	insert: function()
	{
		manager.bst.addChild(parseInt(manager.input_insert.value));
		manager.output_insert.innerHTML = JSON.stringify(manager.bst.root);
	},
	delete: function()
	{
		manager.bst.deleteChild(parseInt(manager.input_delete.value));
		manager.output_delete.innerHTML = JSON.stringify(manager.bst.root);
	}
}


$( document ).ready(function() 
{
	manager.init();
});